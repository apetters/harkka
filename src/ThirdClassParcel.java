
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author antepettersson
 */
public class ThirdClassParcel extends Parcel{

    //pretty unsafe, unlimited distance, big, slow, cheap
    private final static double maxWeight = 50.0;
    public ThirdClassParcel(double weight)  throws OverweightException {
        super (3, "Third class parcel", "Cheap and big, unlimited distance", 1.0, 1.0, 1.0, weight, maxWeight, -1, 1.0, 0.005);
        if (weight>maxWeight)
            throw new OverweightException();
            
    }
    
  
    
    @Override
    public void bump(int gs) {
        //heavy items are not bumped so easily
        Random r = new Random();
        if (this.item.getDimensions().get(3)< r.nextDouble()*maxWeight) //check if item weight is less than random bump value
            this.item.bump();
    }

    //5mm padding, max dimensions 1*1*1 m 
}
