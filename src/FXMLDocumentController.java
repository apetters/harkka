/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.web.WebView;

/**
 *
 * @author antepettersson
 */
public class FXMLDocumentController implements Initializable {
    TimoSystem timo;
    
    private Label label;
    @FXML
    private Button button;
    @FXML
    private WebView webViewer;
    @FXML
    private ComboBox<String> mapCityComboBox;
    @FXML
    private ToggleGroup paketit;
    @FXML
    private ComboBox<Item> itemComboBox;
    @FXML
    private ComboBox<String> fromCityBox;
    @FXML
    private ComboBox<Smartpost> fromSmartpostBox;
    @FXML
    private ComboBox<String> toCityBox;
    @FXML
    private ComboBox<Smartpost> toSmartpostBox;
    
    private ArrayList<Item> items;
    @FXML
    private TextArea itemDescriptionTextArea;
    @FXML
    private ListView<Item> itemsListView;
    @FXML
    private TextField newItemNameBox;
    @FXML
    private TextField newItemXBox;
    @FXML
    private TextField newItemYBox;
    @FXML
    private TextField newItemZBox;
    @FXML
    private TextField newItemMBox;
    @FXML
    private CheckBox newItemFragileCheck;
    @FXML
    private TextArea newItemDescriptionBox;
    @FXML
    private Button createNewItem;
    @FXML
    private Button sendButton;
    @FXML
    private Button emptyButton;
    @FXML
    private Label numberOfPacketsLabel;
    @FXML
    private Button createPackageButton;
    @FXML
    private RadioButton toggle1;
    @FXML
    private RadioButton toggle2;
    @FXML
    private RadioButton toggle3;
    @FXML
    private TextArea outputTextArea;
    @FXML
    private TextArea packageTextArea;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {

        displaySmartposts(mapCityComboBox.getValue());
    }
    
    public void displaySmartposts(String city){
        //Place all smartposts in a certain city on the map
        for (Smartpost s : timo.getSmartposts()){
                if (s.getCity().equalsIgnoreCase(city)){
                    String arg1 = "'"+s.getAddress()+","+s.getCode()+" "+s.getCity()+"'";
                    String arg2 = "'"+s.getName()+", aukioloajat: "+s.getAvailability()+"'";
                    String arg3 = "'blue'";
                    Object result = webViewer.getEngine().executeScript("document.goToLocation("+arg1+","+arg2+","+arg3+")");
                    System.out.println(s.getName()+" status: " + result);
                }
        }
    }
    
  
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        timo = new TimoSystem();
        timo.loadFromItella();
        
        webViewer.getEngine().load(getClass().getResource("index.html").toExternalForm());
        for (String s : timo.getCities()){
            mapCityComboBox.getItems().add(s);
            fromCityBox.getItems().add(s);
            toCityBox.getItems().add(s);
        }
        //create some mailable items
        items=new ArrayList<>();
        items.add(new Item("Pokegochi", "Pokegochi - the virtual fighting pet", true, 0.1, 0.2, 0.01, 0.050)); 
        items.add(new Item("Roll of duct tape", "As seen on mythbusters, ain't nuthin' it can't fix",false,0.15,0.15,0.05,0.100));
        items.add(new Item("W54", "Decommissioned suitcase nuke", true, 0.27,0.27,0.4,23));
        items.add(new Item("Item 4","The mysterious fourth item - nobody knows what it's for", true, 0.1,0.1,0.1,0.1));
        
        refreshItems();
        toggle1.setUserData(1);
        toggle2.setUserData(2);
        toggle3.setUserData(3);
        
        outputTextArea.setText("Tervetuloa käyttämään TIMO-automaattia!\n");
        //set text area to autoscroll to bottom
        //this block is based on code from http://stackoverflow.com/questions/17799160/javafx-textarea-and-autoscroll
        //however, modified untill it actually works with the current javaFX..
        outputTextArea.textProperty().addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
                outputTextArea.setScrollTop(Double.MAX_VALUE); //this will scroll to the bottom.
            }
        });
    }    

    
    private void refreshItems(){
    itemComboBox.getItems().clear();
    itemsListView.getItems().clear();
        for (Item i: items){
        itemComboBox.getItems().add(i);
        itemsListView.getItems().add(i);
    }
    }
    @FXML
    private void fromCityChange(ActionEvent event) {
        //populate from-Smartpost combobox
        String city = fromCityBox.getValue();
        fromSmartpostBox.getItems().clear();
        for (Smartpost s : timo.getSmartposts())
            if ((s.getCity().equalsIgnoreCase(city)))
                fromSmartpostBox.getItems().add(s);
    }

    @FXML
    private void fromSmartpostChange(ActionEvent event) {
    }

    @FXML
    private void toCityChange(ActionEvent event) {
        //populate from-Smartpost combobox
        String city = toCityBox.getValue();
        toSmartpostBox.getItems().clear();
        for (Smartpost s : timo.getSmartposts())
            if ((s.getCity().equalsIgnoreCase(city)))
                toSmartpostBox.getItems().add(s);
    }

    @FXML
    private void toSmartpostChange(ActionEvent event) {
    }

    @FXML
    private void itemComboBoxChange(ActionEvent event) {
        itemDescriptionTextArea.setText(itemComboBox.getValue().getDescription());
    }

    @FXML
    private void createNewItemAction(ActionEvent event) {
        String n=newItemNameBox.getText();
        String d=newItemDescriptionBox.getText();
        Boolean f=newItemFragileCheck.isSelected();
        double x=new Float(newItemXBox.getText());
        double y=new Float(newItemYBox.getText());
        double z=new Float(newItemZBox.getText());
        double w=new Float(newItemMBox.getText());
        items.add(new Item(n,d,f,x,y,z,w));
        refreshItems();
    }

    @FXML
    private void sendButtonAction(ActionEvent event) {
        for (Parcel p : timo.getPackets().asArrayList()){
            //these are just convenience variables for constructing the
            //argument for the js-script
            Location start=p.getFrom().getLocation();
            Location stop=p.getTo().getLocation();
            ArrayList<String> args = new ArrayList<>();
            args.add(String.valueOf(start.getLat()));
            args.add(String.valueOf(start.getLon()));
            args.add(String.valueOf(stop.getLat()));
            args.add(String.valueOf(stop.getLon()));
            int cn = p.getClassNumber();
            //So the next line is not pretty, but it's effective and fun
            //if class number == 1 then colour = red
            //if == 2 then green
            //else blue
            String colour = (cn == 1)? "red" : ((cn == 2)? "green" : "blue");
            //launch js-script
            double dist = (double) webViewer.getEngine().executeScript("document.createPath("+args+",'"+colour+"',"+cn+")");
            //output result
            String result = "Toimitus valmis. Sisältö: "+ p.getItem().getName() + ". ";
            if (dist>p.getMaxDistance() && p.getMaxDistance()!=-1){
                result = result + "Valitettavasti matka "+dist+"km oli timomiehille liian pitkä, eikä paketti päätynyt perille.\n";
            } else {
                for (int i=0; i<= dist % 10; i++){
                    p.bump(2); //give packet a 2g bump every 10km;
                }
                result = result + "Paketti on haettavissa automaatista: " + p.getTo().getName() + ", " + p.getTo().getAddress();
                if (p.getItem().getBroken()){
                    result = result + ". Valitettavasti esine on särkynyt matkan varrella. Timo ei vastaa esineille mahdollisesti tulleista vahingoista.\n";
                } else result=result+'\n';}
        outputTextArea.appendText(result);//System.out.println(result);
            
        }
        timo.getPackets().clear();
        numberOfPacketsLabel.setText("Paketteja varastossa: "+timo.getPackets().getNumber());
    }

    @FXML
    private void emptyButtonAction(ActionEvent event) {
        webViewer.getEngine().executeScript("document.deletePaths()");
        numberOfPacketsLabel.setText("Paketteja varastossa: "+timo.getPackets().getNumber());
    }

    @FXML
    private void createPackageButtonAction(ActionEvent event) {
        Parcel p = null;
        if (itemComboBox.getValue() == null ){ //no selection of item
            packageTextArea.appendText("Valitse lähetettävä esine\n");
            return;
        }
        //use clone constructor, because selected item may be modified (eg broken)
        Item i = new Item(itemComboBox.getValue());
        Smartpost from = fromSmartpostBox.getValue();
        Smartpost to = toSmartpostBox.getValue();
        
        if (from == null || to == null) { // no selection of Smartpost
            packageTextArea.appendText("Valitse sekä lähtö- että vastaanottoautomaatti\n");
            return;
        }
        if (to.equals(from)){
            packageTextArea.appendText("Lähtö- ja vastaanottoautomaatti ei voi olla sama\n");
            return;
        }
        int packetClass = (int) paketit.getSelectedToggle().getUserData();
        try{
            switch (packetClass){
                case 1:
                    p = new FirstClassParcel(i.getDimensions().get(3)); //nr 3 is weight
                    p.setItem(i);
                    p.setFrom(from);
                    p.setTo(to);
                    timo.getPackets().add(p);
                    packageTextArea.appendText("1. luokan paketti lisätty varastoon\n");
                    packageTextArea.appendText("Paketteja varastossa: "+timo.getPackets().getNumber()+'\n');
                    break;
                case 2:
                    p = new SecondClassParcel(i.getDimensions().get(3)); //nr 3 is weight
                    p.setItem(i);
                    p.setFrom(from);
                    p.setTo(to);
                    timo.getPackets().add(p);
                    packageTextArea.appendText("2. luokan paketti lisätty varastoon\n");
                    packageTextArea.appendText("Paketteja varastossa: "+timo.getPackets().getNumber()+'\n');
                    break;
                case 3:
                    p = new ThirdClassParcel(i.getDimensions().get(3)); //nr 3 is weight
                    p.setItem(i);
                    p.setFrom(from);
                    p.setTo(to);
                    timo.getPackets().add(p);
                    packageTextArea.appendText("3. luokan paketti lisätty varastoon\n");
                    packageTextArea.appendText("Paketteja varastossa: "+timo.getPackets().getNumber()+'\n');
                    break;
            }
            
        }catch (OverweightException ex){
            packageTextArea.appendText("Valittu esine on liian painava valitulle pakettiluokalle\n");
        }
        
        numberOfPacketsLabel.setText("Paketteja varastossa: "+timo.getPackets().getNumber() + '\n');
    }

    @FXML
    private void switchTabAction(Event event) {
        packageTextArea.setText("Paketteja varastossa: "+timo.getPackets().getNumber() + '\n');
    }
    
}
