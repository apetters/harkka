
import org.w3c.dom.Element;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author antepettersson
 */
public class Smartpost {
    private Location location;
    private String code;
    private String city;
    private String address;
    private String name;
    private String availability;
    
    public Smartpost(double lat, double lon, String na, String ad, String co, String ci, String av) {
        location = new Location(lat,lon);
        name = na;
        address = ad;
        code = co;
        city = ci;
        availability = av;
    }
    
    public Location getLocation(){
        return location;
    }
    
    public String getCode(){
        return code;
    }
    
    public String getCity(){
        return city;
    }
    
    public String getAddress(){
        return address;
    }
    
    public String getName(){
        return name;
    }
    
    public String getAvailability(){
        return availability;
    }
    
    @Override
    public String toString(){
        return name + " : "+ address;
    }
    
    public Smartpost (Element e){
        
        // a version of the constructor that takes as an argument an XML-element 
        // formed according to http://smartpost.ee/fi_apt.xml (<location>)
        // this functionality could arguably be located in class TimoSystem
        // for better encapsulation
        
        location = new Location (new Double(e.getElementsByTagName("lat").item(0).getTextContent()), 
                                 new Double(e.getElementsByTagName("lng").item(0).getTextContent()));
        name = e.getElementsByTagName("postoffice").item(0).getTextContent();
        address = e.getElementsByTagName("address").item(0).getTextContent();
        code = e.getElementsByTagName("code").item(0).getTextContent();
        city = e.getElementsByTagName("city").item(0).getTextContent();
        availability = e.getElementsByTagName("availability").item(0).getTextContent();
    }
}
